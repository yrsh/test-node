# Test

This service loads the price of BTCEUR and applies commission to it.

## Configuration
- PORT: app port
- HOST: app host
- API: URL to the endpoint with prices
- COM: commission size (%)
- INT: polling interval (ms)

## Run
```
API=https://api.binance.com/api/v3/ticker/bookTicker?symbol=BTCEUR npm start
```
Route for price: /price.  
Example: http://0.0.0.0:3000/price

## Run in docker
Compose is used for convenience. Configuration example in compose file.
```
docker-compose build && docker-compose up
```