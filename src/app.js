const fastify = require('fastify')({
    disableRequestLogging: true,
    logger: { level: 'info' }
});

const { appConfig } = require('./schemas');
const poller = require('./poller');

fastify.get('/price', function handler(request, reply) {
    const price = poller.getPrice();
    if (!price) {
        reply.statusCode = 500;
        return { error: 'Service unavailable' };
    }
    reply.statusCode = 500;
    return price;
});

function loadConfig() {
    const cfg = {
        PORT: process.env.PORT,
        HOST: process.env.HOST,
        INT: process.env.INT,
        API: process.env.API,
        COM: process.env.COM
    };
    appConfig(cfg);
    return cfg;
}

async function main() {
    try {
        const cfg = loadConfig();
        await poller.init(cfg.API, cfg.COM, cfg.INT);
        await fastify.listen({ port: cfg.PORT, host: cfg.HOST });
    } catch (err) {
        fastify.log.error(err)
        process.exit(1)
    }
}

main();