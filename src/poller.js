const superagent = require('superagent');
const logger = require('pino')();

const { binanceRsBody } = require('./schemas');

const state = {
    data: null,
    pending: false,
    result: null
};

// Simple API polling
async function check(api, commission) {
    if (state.pending) {
        return;
    }
    try {
        state.pending = true;
        const res = await superagent.get(api);
        binanceRsBody(res.body);
        state.data = res.body;
        applyCommission(commission);
    } catch (err) {
        logger.error(err);
    } finally {
        state.pending = false;
    }
}

function applyCommission(commission) {
    // Buyers pay commission
    const bid = state.data.bidPrice * (1 + commission / 100);
    // Sellers sell with commission
    const ask = state.data.askPrice * (1 - commission / 100);
    const mid = (bid + ask) / 2;
    state.result = {
        ask: ask.toFixed(8),
        bid: bid.toFixed(8),
        mid: mid.toFixed(8),
        upd: Date.now() // timestamp of last update
    };
}

async function init(api, commission, interval) {
    // Initial state
    await check(api, commission);
    // Polling
    setInterval(() => check(api, commission), interval);
}

module.exports = {
    init,
    getPrice: () => state.result
};