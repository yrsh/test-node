const Ajv = require("ajv");
const ajv = new Ajv({ useDefaults: true });

// JSON schemas for structure validation

// Custom string format (binance numbers are strings)
ajv.addFormat('numStr', {
    type: 'string',
    validate: (val) => !Number.isNaN(Number(val)),
});

// Replace strings by numbers
function toNumbers(obj, keys) {
    keys.forEach(k => obj[k] = Number(obj[k]));
}

// Validation & transformation
function makeValidator(schema, nums) {
    const validate = ajv.compile(schema);
    return (val) => {
        const isValid = validate(val);
        toNumbers(val, nums);
        if (!isValid) {
            throw new Error(JSON.stringify(validate.errors[0]));
        }
    }
}

const numStr = { type: 'string', format: 'numStr' }; // String which represents valid number
const binanceRsBody = makeValidator({
    type: 'object',
    properties: {
        symbol: { type: 'string', enum: ['BTCEUR'] },
        bidPrice: numStr,
        bidQty: numStr,
        askPrice: numStr,
        askQty: numStr
    },
    required: ['symbol', 'bidPrice', 'bidQty', 'askPrice', 'askQty']
}, [ 'bidPrice', 'bidQty', 'askPrice', 'askQty' ]);

const appConfig = makeValidator({
    type: 'object',
    properties: {
        PORT: { type: 'string', format: 'numStr', default: '3000' },
        HOST: { type: 'string', default: '0.0.0.0' },
        API: { type: 'string' },
        INT: { type: 'string', format: 'numStr', default: '10000' },
        COM: { type: 'string', format: 'numStr', default: '0.01' },
    },
    required: ['API', 'INT' ]
}, [ 'PORT', 'INT', 'COM' ]);

module.exports = {
    binanceRsBody,
    appConfig
};